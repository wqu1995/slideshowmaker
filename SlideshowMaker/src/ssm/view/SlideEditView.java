package ssm.view;

import java.io.File;
import java.net.URL;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.LanguagePropertyType.ERROR_IO_FAIL;
import static ssm.LanguagePropertyType.ERROR_IO_LOAD_FAIL;
import static ssm.LanguagePropertyType.ERROR_IO_LOAD_IMAGE_FAIL;
import static ssm.LanguagePropertyType.ERROR_TITLE;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import static ssm.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import ssm.controller.FileController;
import ssm.controller.ImageSelectionController;
import ssm.error.ErrorHandler;
import ssm.model.Slide;
import static ssm.file.SlideShowFileManager.SLASH;

/**
 * This UI component has the controls for editing a single slide
 * in a slide show, including controls for selected the slide image
 * and changing its caption.
 * 
 * @author McKilla Gorilla & Wenjun Qu
 */
public class SlideEditView extends HBox {
    // SLIDE THIS COMPONENT EDITS
    Slide slide;
    FileController fileController;
    
    
    
    // DISPLAYS THE IMAGE FOR THIS SLIDE
    ImageView imageSelectionView;
    
    // CONTROLS FOR EDITING THE CAPTION
    VBox captionVBox;
    Label captionLabel;
    TextField captionTextField;
    
    // PROVIDES RESPONSES FOR IMAGE SELECTION
    ImageSelectionController imageController;
    
    //protected double scaledWidth, scaledHeight;
    boolean selected = false;

    /**
     * THis constructor initializes the full UI for this component, using
     * the initSlide data for initializing values./
     * 
     * @param initSlide The slide to be edited by this component.
     */
    public SlideEditView(Slide initSlide, FileController controller) {
        fileController = controller;
	// FIRST SELECT THE CSS STYLE CLASS FOR THIS CONTAINER
	this.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);

	// KEEP THE SLIDE FOR LATER
	slide = initSlide;
	
	// MAKE SURE WE ARE DISPLAYING THE PROPER IMAGE
	imageSelectionView = new ImageView();
	updateSlideImage();

	// SETUP THE CAPTION CONTROLS
	captionVBox = new VBox();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	captionLabel = new Label(props.getProperty(LanguagePropertyType.LABEL_CAPTION));
	captionTextField = new TextField();
        //captionTextField.setText("1");
        if(slide.getCaption() == null){
            slide.setImageCaption(" ");
        }
        //slide.setImageCaption(" ");
	captionVBox.getChildren().add(captionLabel);
	captionVBox.getChildren().add(captionTextField);
	// LAY EVERYTHING OUT INSIDE THIS COMPONENT
	getChildren().add(imageSelectionView);
	getChildren().add(captionVBox);

	// SETUP THE EVENT HANDLERS
	imageController = new ImageSelectionController();
	imageSelectionView.setOnMouseClicked(e -> {
                imageController.processSelectImage(slide, this);
                fileController.markAsEdited();
	});        
        captionTextField.textProperty().addListener((observable, oldValue, newValue) ->{
            slide.setImageCaption(newValue);
            fileController.markAsEdited();
        });
    }

    /////////////////////////CHANGE
    public void setCaptionText(String initCaption){
        captionTextField.setText(initCaption);
    }
    

    
    /**
     * This function gets the image for the slide and uses it to
     * update the image displayed.
     */
    public void updateSlideImage() {
                PropertiesManager props = PropertiesManager.getPropertiesManager();

	String imagePath = slide.getImagePath() + SLASH + slide.getImageFileName();
	File file = new File(imagePath);
	try {
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    Image slideImage = new Image(fileURL.toExternalForm());
	    imageSelectionView.setImage(slideImage);
	    
	    // AND RESIZE IT
	    double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
	    double perc = scaledWidth / slideImage.getWidth();
	    double scaledHeight = slideImage.getHeight() * perc;
	    imageSelectionView.setFitWidth(scaledWidth);
	    imageSelectionView.setFitHeight(scaledHeight);

	} catch (Exception e) {
	    Alert error = new Alert(Alert.AlertType.ERROR);
            error.setTitle(props.getProperty(ERROR_TITLE));
            error.setHeaderText(props.getProperty(ERROR_IO_FAIL));
            error.setContentText(props.getProperty(ERROR_IO_LOAD_IMAGE_FAIL));
            error.showAndWait();
	}
    }    
}