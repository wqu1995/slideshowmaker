
package ssm.view;

import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.TOOLTIP_NEXT_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_PREVIOUS_SLIDE;
import static ssm.LanguagePropertyType.VIEW_TITLE_WINDOW;
import static ssm.StartupConstants.ICON_NEXT;
import static ssm.StartupConstants.ICON_PREVIOUS;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.VIEW_WINDOW_ICON;
import static ssm.StartupConstants.WINDOW_ICON;
import ssm.controller.SlideViewController;
import ssm.model.Slide;
import ssm.model.SlideShowModel;

/**
 * This UI component for viewing slide in a different window. It has the controls
 * for loading the next or previous slide/
 * @author Wenjun
 */
public class SlideView {
    SlideShowModel slideToShow;
    SlideViewController slideController;
    Stage slideStage = new Stage();
    Scene slideScene;
    BorderPane slideViewPane = new BorderPane();
    HBox slideToView, slideCaption;
    Slide currentSlide;
    
    
    public SlideView(SlideShowMakerView initUI){
        slideToShow = initUI.getSlideShow();
    }
    
    public SlideShowModel getViewSlides(){
        return slideToShow;
    }
    
    public void startView(){
        currentSlide = slideToShow.getSlides().get(0);

        slideToView = new HBox();
        slideCaption = new HBox();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        ObservableList<Slide> slides = slideToShow.getSlides();
        

        slideScene = new Scene(slideViewPane);
        slideStage.setTitle(props.getProperty(VIEW_TITLE_WINDOW));
        slideStage.getIcons().add(new Image("file:" +PATH_ICONS + VIEW_WINDOW_ICON));

        
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        slideStage.setX(bounds.getMinX());
        slideStage.setY(bounds.getMinY());
        slideStage.setWidth(bounds.getWidth());
        slideStage.setHeight(bounds.getHeight());
        
        HBox slideTitle = new HBox();
        StackPane titlePane = new StackPane();
        Text titleText = new Text(slideToShow.getTitle());
        titleText.setFont(Font.font("Cambria",40));

        titlePane.getChildren().add(titleText);
        titlePane.setAlignment(Pos.CENTER);
        slideTitle.getChildren().add(titlePane);
        HBox.setHgrow(titlePane, Priority.ALWAYS);
        
        HBox previousButtonBar = new HBox();
        HBox nextButtonBar = new HBox();
        StackPane previousbuttonPane = new StackPane();
        StackPane nextbuttonPane = new StackPane();
        
        Button previousButton = new Button();
        previousButton.setGraphic(new ImageView(new Image("file:" + PATH_ICONS + ICON_PREVIOUS)));
        previousButton.setTooltip(new Tooltip(props.getProperty(TOOLTIP_PREVIOUS_SLIDE)));
        previousButton.setPadding(new Insets(20,20,20,20));
        previousbuttonPane.getChildren().add(previousButton);
        previousbuttonPane.setAlignment(Pos.CENTER);
        previousButtonBar.getChildren().add(previousbuttonPane);
        HBox.setHgrow(previousbuttonPane, Priority.ALWAYS);
        
        Button nextButton = new Button();
        nextButton.setGraphic(new ImageView(new Image("file:" + PATH_ICONS + ICON_NEXT)));
        nextButton.setTooltip(new Tooltip(props.getProperty(TOOLTIP_NEXT_SLIDE)));
        nextButton.setPadding(new Insets(20,20,20,20));
        nextbuttonPane.getChildren().add(nextButton);
        nextbuttonPane.setAlignment(Pos.CENTER);
        nextButtonBar.getChildren().add(nextbuttonPane);
        HBox.setHgrow(nextbuttonPane, Priority.ALWAYS);
        
        slideController = new SlideViewController(this);
        previousButton.setOnAction(e->{
            slideController.processPreviousRequest();
        });
        nextButton.setOnAction(e->{
            slideController.processNextRequest();
        });

        slideViewPane.setTop(slideTitle);
        slideViewPane.setCenter(slideToView);
        slideViewPane.setLeft(previousButtonBar);
        slideViewPane.setRight(nextButtonBar);
        slideViewPane.setBottom(slideCaption);
        slideStage.setScene(slideScene);
        slideStage.show();
        displaySlide();    
    }
    
    public Slide getCurrentSlide(){
        return currentSlide;
    }
    public void setCurrentSlide(Slide slide){
        currentSlide = slide;
    }
    public void displaySlide() {
        slideToView.getChildren().clear();
        slideCaption.getChildren().clear();
        StackPane captionLabel = new StackPane();
        StackPane imagePane = new StackPane();
        ImageView slideToDisplay = new ImageView(new Image("file:" +currentSlide.getImagePath() + "/" + currentSlide.getImageFileName()));
        //slideToDisplay.setFitHeight(900);
        if(slideToDisplay.getImage().getHeight()>= (slideStage.getHeight()-100)){
            slideToDisplay.setFitHeight(slideStage.getHeight()-200);
        }
        if(slideToDisplay.getImage().getWidth() >= (slideStage.getWidth()-100)){
            slideToDisplay.setFitWidth(slideStage.getWidth()-200);
        }
        imagePane.getChildren().add(slideToDisplay);
        imagePane.setAlignment(Pos.CENTER);
        slideToView.getChildren().add(imagePane);
        HBox.setHgrow(imagePane, Priority.ALWAYS);
        
        Text caption = new Text(currentSlide.getCaption());
        caption.setFont(Font.font("Cambria",30));
        caption.setFill(Color.RED);
        captionLabel.getChildren().add(caption);
        captionLabel.setAlignment(Pos.TOP_CENTER);
        slideCaption.getChildren().add(captionLabel);
        HBox.setHgrow(captionLabel, Priority.ALWAYS);
    }
}
