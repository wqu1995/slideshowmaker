package ssm.controller;

import java.io.File;
import java.io.IOException;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.stage.FileChooser;
import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.ERROR_IO_FAIL;
import static ssm.LanguagePropertyType.ERROR_IO_LOAD_FAIL;
import static ssm.LanguagePropertyType.ERROR_IO_SAVE_FAIL;
import static ssm.LanguagePropertyType.ERROR_TITLE;
import static ssm.LanguagePropertyType.PROMPT_TO_LOAD_SUCCESS;
import static ssm.LanguagePropertyType.PROMPT_TO_NEW_WORK;
import static ssm.LanguagePropertyType.PROMPT_TO_SAVE_BUTTON_CANCEL;
import static ssm.LanguagePropertyType.PROMPT_TO_SAVE_BUTTON_NOT_SAVE;
import static ssm.LanguagePropertyType.PROMPT_TO_SAVE_BUTTON_SAVE;
import static ssm.LanguagePropertyType.PROMPT_TO_SAVE_CONTENT;
import static ssm.LanguagePropertyType.PROMPT_TO_SAVE_HEADER;
import static ssm.LanguagePropertyType.TITLE_WINDOW;
import ssm.model.SlideShowModel;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;
import ssm.view.SlideShowMakerView;
import static ssm.StartupConstants.PATH_SLIDE_SHOWS;
import ssm.view.SlideView;
/**
 * This class serves as the controller for all file toolbar operations,
 * driving the loading and saving of slide shows, among other things.
 * 
 * @author McKilla Gorilla & Wenjun Qu
 */
public class FileController {

    // WE WANT TO KEEP TRACK OF WHEN SOMETHING HAS NOT BEEN SAVED
    private boolean saved;

    // THE APP UI
    private SlideShowMakerView ui;
    
    // THIS GUY KNOWS HOW TO READ AND WRITE SLIDE SHOW DATA
    private SlideShowFileManager slideShowIO;

    /**
     * This default constructor starts the program without a slide show file being
     * edited.
     *
     * @param initSlideShowIO The object that will be reading and writing slide show
     * data.
     */
    public FileController(SlideShowMakerView initUI, SlideShowFileManager initSlideShowIO) {
        // NOTHING YET
        saved = true;
	ui = initUI;
        slideShowIO = initSlideShowIO;
    }
    
    public void markAsEdited() {
        saved = false;
        ui.updateToolbarControls(saved);
    }

    /**
     * This method starts the process of editing a new slide show. If a pose is
     * already being edited, it will prompt the user to save it first.
     */
    public void handleNewSlideShowRequest() {
            PropertiesManager props = PropertiesManager.getPropertiesManager();

            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToMakeNew = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                Alert save = new Alert(AlertType.CONFIRMATION);
                save.setTitle(props.getProperty(TITLE_WINDOW));
                save.setHeaderText(props.getProperty(PROMPT_TO_SAVE_HEADER));
                save.setContentText(props.getProperty(PROMPT_TO_SAVE_CONTENT));
                ButtonType buttonOne = new ButtonType(props.getProperty(PROMPT_TO_SAVE_BUTTON_SAVE));
                ButtonType buttonTwo = new ButtonType(props.getProperty(PROMPT_TO_SAVE_BUTTON_NOT_SAVE));
                ButtonType buttonCancel = new ButtonType(props.getProperty(PROMPT_TO_SAVE_BUTTON_CANCEL), ButtonData.CANCEL_CLOSE);
                save.getButtonTypes().setAll(buttonOne, buttonTwo, buttonCancel);
                Optional<ButtonType> result = save.showAndWait();
                if(result.get() == buttonOne){
                    try{
                        continueToMakeNew = promptToSave();
                    }
                    catch(IOException ioe){
                        ErrorHandler eH = ui.getErrorHandler();
                        eH.processError(ERROR_TITLE, ERROR_IO_FAIL, ERROR_IO_SAVE_FAIL);
                        return;
                    }
                }
                
                else if(result.get() == buttonTwo){
                    continueToMakeNew = true;
                }
                else if (result.get() == buttonCancel){
                    return;
                }
                else if(saved){
                    continueToMakeNew = saved;
                }
            }

            // IF THE USER REALLY WANTS TO MAKE A NEW COURSE
            if (continueToMakeNew) {
                // RESET THE DATA, WHICH SHOULD TRIGGER A RESET OF THE UI
                SlideShowModel slideShow = ui.getSlideShow();
		slideShow.reset();
                saved = false;

                // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
                // THE APPROPRIATE CONTROLS
                ui.updateToolbarControls(saved);

                // TELL THE USER THE SLIDE SHOW HAS BEEN CREATED
                Alert newWork = new Alert(AlertType.INFORMATION);
                newWork.setTitle(props.getProperty(TITLE_WINDOW));
                newWork.setHeaderText(null);
                newWork.setContentText(props.getProperty(PROMPT_TO_NEW_WORK));
                newWork.showAndWait();
                ui.reloadSlideShowPane(slideShow);
            }
        
    }

    /**
     * This method lets the user open a slideshow saved to a file. It will also
     * make sure data for the current slideshow is not lost.
     */
    public void handleLoadSlideShowRequest() {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToOpen = true;
            PropertiesManager props = PropertiesManager.getPropertiesManager();

            if (!saved) {

                Alert save = new Alert(AlertType.CONFIRMATION);
                save.setTitle(props.getProperty(TITLE_WINDOW));
                save.setHeaderText(props.getProperty(PROMPT_TO_SAVE_HEADER));
                save.setContentText(props.getProperty(PROMPT_TO_SAVE_CONTENT));
                ButtonType buttonOne = new ButtonType(props.getProperty(PROMPT_TO_SAVE_BUTTON_SAVE));
                ButtonType buttonTwo = new ButtonType(props.getProperty(PROMPT_TO_SAVE_BUTTON_NOT_SAVE));
                ButtonType buttonCancel = new ButtonType(props.getProperty(PROMPT_TO_SAVE_BUTTON_CANCEL), ButtonData.CANCEL_CLOSE);
                save.getButtonTypes().setAll(buttonOne, buttonTwo, buttonCancel);
                Optional<ButtonType> result = save.showAndWait();
                if(result.get() == buttonOne){
                    try{
                        continueToOpen = promptToSave();
                    }
                    catch(IOException ioe){
                        ErrorHandler eH = ui.getErrorHandler();
                        eH.processError(ERROR_TITLE, ERROR_IO_FAIL, ERROR_IO_SAVE_FAIL);
                        return;
                    }
                }
                
                else if(result.get() == buttonTwo){
                    continueToOpen = true;
                }
                else if (result.get() == buttonCancel){
                    return;
                }
            }

            // IF THE USER REALLY WANTS TO OPEN A POSE
            if (continueToOpen) {
                // GO AHEAD AND PROCEED MAKING A NEW POSE
                promptToOpen();
                
            }

    }

    /**
     * This method will save the current slideshow to a file. Note that we already
     * know the name of the file, so we won't need to prompt the user.
     */
    public boolean handleSaveSlideShowRequest() {
        try {
	    // GET THE SLIDE SHOW TO SAVE
	    SlideShowModel slideShowToSave = ui.getSlideShow();
	    
            // SAVE IT TO A FILE
            slideShowIO.saveSlideShow(slideShowToSave);

            // MARK IT AS SAVED
            saved = true;

            // AND REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
            // THE APPROPRIATE CONTROLS
            ui.updateToolbarControls(saved);
            
	    return true;
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(ERROR_TITLE, ERROR_IO_FAIL, ERROR_IO_SAVE_FAIL);
	    return false;
        }
    }

    public void hadleViewSlideShowRequest(){
        SlideShowModel slideToShow = ui.getSlideShow();
        if(slideToShow == null){
            System.out.println("no slide");
        }
        SlideView viewWindow = new SlideView(ui);
        viewWindow.startView();
        //viewWindow.displaySlide(slideToShow.getSlides().get(0));
    }
    
     /**
     * This method will exit the application, making sure the user doesn't lose
     * any data first.
     */
    public void handleExitRequest() {
                    PropertiesManager props = PropertiesManager.getPropertiesManager();

            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToExit = true;
            if (!saved) {
                Alert save = new Alert(AlertType.CONFIRMATION);
                save.setTitle(props.getProperty(TITLE_WINDOW));
                save.setHeaderText(props.getProperty(PROMPT_TO_SAVE_HEADER));
                save.setContentText(props.getProperty(PROMPT_TO_SAVE_CONTENT));
                ButtonType buttonOne = new ButtonType(props.getProperty(PROMPT_TO_SAVE_BUTTON_SAVE));
                ButtonType buttonTwo = new ButtonType(props.getProperty(PROMPT_TO_SAVE_BUTTON_NOT_SAVE));
                ButtonType buttonCancel = new ButtonType(props.getProperty(PROMPT_TO_SAVE_BUTTON_CANCEL), ButtonData.CANCEL_CLOSE);
                save.getButtonTypes().setAll(buttonOne, buttonTwo, buttonCancel);
                Optional<ButtonType> result = save.showAndWait();
                if(result.get() == buttonOne){
                    try{
                        continueToExit = promptToSave();
                    }
                    catch(IOException ioe){
                        ErrorHandler eH = ui.getErrorHandler();
                        eH.processError(ERROR_TITLE, ERROR_IO_FAIL, ERROR_IO_SAVE_FAIL);
                        return;
                    }
                }
                
                else if(result.get() == buttonTwo){
                    continueToExit = true;
                }
                else if (result.get() == buttonCancel){
                    return;
                }
            }

            // IF THE USER REALLY WANTS TO EXIT THE APP
            if (continueToExit) {
                // EXIT THE APPLICATION
                System.exit(0);
            }
        
        }
    

    /**
     * This helper method verifies that the user really wants to save their
     * unsaved work, which they might not want to do. Note that it could be used
     * in multiple contexts before doing other actions, like creating a new
     * pose, or opening another pose, or exiting. Note that the user will be
     * presented with 3 options: YES, NO, and CANCEL. YES means the user wants
     * to save their work and continue the other action (we return true to
     * denote this), NO means don't save the work but continue with the other
     * action (true is returned), CANCEL means don't save the work and don't
     * continue with the other action (false is retuned).
     *
     * @return true if the user presses the YES option to save, true if the user
     * presses the NO option to not save, false if the user presses the CANCEL
     * option to not continue.
     */
    private boolean promptToSave() throws IOException {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        boolean saveWork = true; // @todo change this to prompt

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (saveWork) {
            SlideShowModel slideShow = ui.getSlideShow();
            slideShowIO.saveSlideShow(slideShow);
            saved = true;
        } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        else if (!true) {
            return false;
        }

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }

    /**
     * This helper method asks the user for a file to open. The user-selected
     * file is then loaded and the GUI updated. Note that if the user cancels
     * the open process, nothing is done. If an error occurs loading the file, a
     * message is displayed, but nothing changes.
     */
    private void promptToOpen() {
        // AND NOW ASK THE USER FOR THE COURSE TO OPEN
        FileChooser slideShowFileChooser = new FileChooser();
        slideShowFileChooser.setInitialDirectory(new File(PATH_SLIDE_SHOWS));
        File selectedFile = slideShowFileChooser.showOpenDialog(ui.getWindow());
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
		SlideShowModel slideShowToLoad = ui.getSlideShow();
                slideShowIO.loadSlideShow(slideShowToLoad, selectedFile.getAbsolutePath());
                ui.reloadSlideShowPane(slideShowToLoad);
                saved = true;
                ui.updateToolbarControls(saved);
            } catch (Exception e) {
                ErrorHandler eH = ui.getErrorHandler();
                eH.processError(ERROR_TITLE, ERROR_IO_FAIL, ERROR_IO_LOAD_FAIL);
                

                // @todo
            }
            Alert loadWork = new Alert(AlertType.INFORMATION);
                loadWork.setTitle(props.getProperty(TITLE_WINDOW));
                loadWork.setHeaderText(null);
                loadWork.setContentText(props.getProperty(PROMPT_TO_LOAD_SUCCESS));
                loadWork.showAndWait();
        }
    }

    /**
     * This mutator method marks the file as not saved, which means that when
     * the user wants to do a file-type operation, we should prompt the user to
     * save current work first. Note that this method should be called any time
     * the pose is changed in some way.
     */
    public void markFileAsNotSaved() {
        saved = false;
    }

    /**
     * Accessor method for checking to see if the current pose has been saved
     * since it was last editing. If the current file matches the pose data,
     * we'll return true, otherwise false.
     *
     * @return true if the current pose is saved to the file, false otherwise.
     */
    public boolean isSaved() {
        return saved;
    }
    
}

