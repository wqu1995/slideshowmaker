package ssm.model;

import java.io.IOException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import ssm.view.SlideShowMakerView;

/**
 * This class manages all the data associated with a slideshow.
 * 
 * @author McKilla Gorilla & Wenjun Qu
 */
public class SlideShowModel {
    SlideShowMakerView ui;
    String title;
    ObservableList<Slide> slides;
    Slide selectedSlide;
    
    public SlideShowModel(SlideShowMakerView initUI) {
	ui = initUI;
	slides = FXCollections.observableArrayList();
	reset();	
    }
    

    // ACCESSOR METHODS
    public boolean isSlideSelected() {
        //return selectedSlide != null;
        return selectedSlide != null;
    }
    
    public ObservableList<Slide> getSlides() {
	return slides;
    }
    
    public Slide getSelectedSlide() {
	return selectedSlide;
    }

    public String getTitle() { 
	return title; 
    }
    
    // MUTATOR METHODS
    public void setSelectedSlide(Slide initSelectedSlide) {
	selectedSlide = initSelectedSlide;
        selectedSlide.selected = true;
    }
    
    public void setTitle(String initTitle) { 
	title = initTitle; 
    }
    
    public void removeSelectedSlide(){
        slides.remove(selectedSlide);
        selectedSlide=null;
        ui.reloadSlideShowPane(this);
        ui.updateSlideEditBarControls(this);
    }
    public void moveupSelectedSlide(){
        int i = 0;
        
            while(slides.get(i) != selectedSlide){
                i++;
            }
            Slide temp = slides.get(i-1);
            slides.set(i-1, selectedSlide);
            slides.set(i, temp);
            ui.reloadSlideShowPane(this);
        
        
    }
    public void movedownSelectedSlide(){
        int i = 0;
        while(slides.get(i) !=selectedSlide){
            i++;
        }
        Slide temp = slides.get(i+1);
        slides.set(i+1, selectedSlide);
        slides.set(i, temp);
        ui.reloadSlideShowPane(this);
    }

    // SERVICE METHODS
    
    /**
     * Resets the slide show to have no slides and a default title.
     */
    public void reset() {
	slides.clear();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	title = props.getProperty(LanguagePropertyType.DEFAULT_SLIDE_SHOW_TITLE);
	selectedSlide = null;
    }

    /**
     * Adds a slide to the slide show with the parameter settings.
     * @param initImageFileName File name of the slide image to add.
     * @param initImagePath File path for the slide image to add.
     */
    
    ////////////////////////////CHANGE
    public void addSlide(  String initImageFileName,
                            String initImagePath,
                            String initCaption){
        Slide slideToAdd = new Slide(initImageFileName, initImagePath);
        slideToAdd.setImageCaption(initCaption);
	slides.add(slideToAdd);
        
	selectedSlide = null;
	ui.reloadSlideShowPane(this);
    }
        
 
}