package ssm;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.application.Application;
import javafx.scene.control.ChoiceDialog;
import javafx.stage.Stage;
import xml_utilities.InvalidXMLFileFormatException;
import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.ERROR_DATA_FILE_LOADING;
import static ssm.LanguagePropertyType.ERROR_TITLE;
import static ssm.LanguagePropertyType.TITLE_WINDOW;
import static ssm.StartupConstants.PATH_DATA;
import static ssm.StartupConstants.PROPERTIES_SCHEMA_FILE_NAME;
import static ssm.StartupConstants.UI_PROPERTIES_FILE_NAME_CN;
import static ssm.StartupConstants.UI_PROPERTIES_FILE_NAME_EN;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;
import ssm.view.SlideShowMakerView;

/**
 * SlideShowMaker is a program for making custom image slideshows. It will allow
 * the user to name their slideshow, select images to use, select captions for
 * the images, and the order of appearance for slides.
 *
 * @author McKilla Gorilla & Wenjun Qu
 */
public class SlideShowMaker extends Application {
    // THIS WILL PERFORM SLIDESHOW READING AND WRITING
    SlideShowFileManager fileManager = new SlideShowFileManager();

    // THIS HAS THE FULL USER INTERFACE AND ONCE IN EVENT
    // HANDLING MODE, BASICALLY IT BECOMES THE FOCAL
    // POINT, RUNNING THE UI AND EVERYTHING ELSE
    SlideShowMakerView ui = new SlideShowMakerView(fileManager);

    @Override
    public void start(Stage primaryStage) throws Exception {
        // LOAD APP SETTINGS INTO THE GUI AND START IT UP
        boolean success = loadProperties();
        if (success) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String appTitle = props.getProperty(TITLE_WINDOW);

	    // NOW START THE UI IN EVENT HANDLING MODE
	    ui.startUI(primaryStage, appTitle);
	} // THERE WAS A PROBLEM LOADING THE PROPERTIES FILE
	else {
	    // LET THE ERROR HANDLER PROVIDE THE RESPONSE
	    ErrorHandler errorHandler = ui.getErrorHandler();
	    errorHandler.processError(LanguagePropertyType.ERROR_SYSTEM_FAIL, LanguagePropertyType.ERROR_TITLE, LanguagePropertyType.ERROR_DATA_FILE_LOADING);
	    System.exit(0);
	}
    }
    
    /**
     * Loads this application's properties file, which has a number of settings
     * for initializing the user interface.
     * 
     * @return true if the properties file was loaded successfully, false otherwise.
     */
    public boolean loadProperties() {
        try {
            // LOAD THE SETTINGS FOR STARTING THE APP
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);

            //Language selection
            List <String> choices = new ArrayList<>();
            choices.add("English");
            choices.add("Chinese");
            
            ChoiceDialog<String> Language = new ChoiceDialog<>("English", choices);
            Language.setTitle("Language");
            Language.setHeaderText(null);
            Language.setContentText("Choice your language: ");
            
            Optional<String>result = Language.showAndWait();
            if((result.isPresent()) && (result.get() == "English")){
                props.loadProperties(UI_PROPERTIES_FILE_NAME_EN, PROPERTIES_SCHEMA_FILE_NAME);
                return true;
            }
            else if((result.isPresent()) && (result.get() == "Chinese")){
                props.loadProperties(UI_PROPERTIES_FILE_NAME_CN, PROPERTIES_SCHEMA_FILE_NAME);
                return true;
            }
            else if(!(result.isPresent())){
                System.exit(0);
            }
            else{
                return false;
            }
       } catch (InvalidXMLFileFormatException ixmlffe) {
            // SOMETHING WENT WRONG INITIALIZING THE XML FILE
           
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(LanguagePropertyType.ERROR_XML_FILE_FAIL, LanguagePropertyType.ERROR_TITLE, LanguagePropertyType.ERROR_PROPERTIES_FILE_LOADING);
            System.exit(0);

            return false;
        } 
       // return false;
        return false;
    }

    /**
     * This is where the application starts execution. We'll load the
     * application properties and then use them to build our user interface and
     * start the window in event handling mode. Once in that mode, all code
     * execution will happen in response to user requests.
     *
     * @param args This application does not use any command line arguments.
     */
    public static void main(String[] args) {
	launch(args);
    }

}
